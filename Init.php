<?php
session_start();
$GLOBALS['config'] = array
    (
        'mysql' => array
                    (
                        'host' => 'localhost',
                        'username' => 'root',
                        'password' => 'sangeeth123456',
                        'Database' => 'test'
                    ),
        'session' => array 
                    (
                        'session_name' => 'email',
                        'token_name' => 'token'
                    )
    );

spl_autoload_register(function($class){
    require_once 'classes/' . $class . '.php';
});

require_once 'functions/Sanitize.php';

