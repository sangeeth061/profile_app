<?php 
  require_once '../Init.php';
  
  if (!isset($_SESSION['email'])) 
  {
    $_SESSION['msg'] = "You must log in first";
    Redirect::to('../includes/Login.php');
  }
  if (isset($_GET['logout'])) 
  {
    session_destroy();
    unset($_SESSION['email']);
    Redirect::to('../includes/Login.php');
  }
  include('Dashcontol.php');
?>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">        
    <link rel="stylesheet" type="text/css" href="../assets/css/dashboard.css">
</head>
<body>
    <header>
    <div class="row">
    <div class="col-md-6">
        <h2>MINDFIRE</h2>
        <h5 style="float:left;">Burn ignorance</h5>
        </div>
        <div class="col-md-12">
        <span style="text-align:right;"><a href="Dashveiw.php?logout='1'"><button class="button" style="width:10%;"><h4>logout</h4></button></a></span>
        </div>
        </div> 
    </header>
    <div class="container-fluid">
        <div class="log">
            <div class="row">
                <h3> Hi &nbsp<?php echo isset($username) ? $username : ''; ?></h3>
                <h4>Your Basic details</h4>
            </div>
            <table class="table table-responsive">
                <tr>
                    <td class="col-md-6">Firstname</td>
                    <td class="col-md-6"><?php echo isset($firstn) ? $firstn : 'optional'; ?></td>
                </tr>
                <tr>
                    <td class="col-md-6">Lastname</td>
                    <td class="col-md-6"><?php echo isset($firstn) ? $lastn : 'optional'; ?></td>
                </tr>
                <tr>
                <td class="col-md-6">Username</td>
                <td class="col-md-6"><?php echo isset($username) ? $username : 'optional'; ?></td>
                </tr>
                <tr>
                <td class="col-md-6">Gender</td>
                <td class="col-md-6"><?php echo isset($gender) ? $gender : 'optional'; ?></td>
                </tr>
                <tr>
                <td class="col-md-6">Address</td>
                <td class="col-md-6"><?php echo isset($address) ? $address : 'optional'; ?></td>
                </tr>
                <tr>
                <td class="col-md-6">Mobileno</td>
                <td class="col-md-6"><?php echo isset($phnno) ? $phnno : ''; ?></td>
                </tr>
                <tr>
                <td class="col-md-6">City</td>
                <td class="col-md-6"><?php echo isset($city) ? $city : 'optional'; ?></td>
                </tr>
            </table>
        </div>
        <a href="Dashedit.php"><button class="button">Edit</button></a>
    </div>
  </body>
</html>
