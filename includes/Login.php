<?php
require_once '../Init.php';

if (Input::exists())
{
    $validate = new Validate();
    $validation = $validate->check($_POST, array(
                                            'email'  => array(
                                                            'required1' => true,
                                                            'valid-email' => 'email'
                                                        ),
                                            'password' => array(
                                                            'required1' => true,
                                                            'min' => 8
                                                        )
                                            ) 
                                );
    if ($validation->passed())
    {
        $user = new User();
        $login = $user->login(Input::get('email'),Input::get('password'));
        if($login)
        {
            echo 'sucess';
            $_SESSION['email'] = input::get('email');
            Redirect::to('Dashveiw.php');
        }
        else
        {
            echo"check your credentials";
        }
    }
    else
        foreach ($validation->errors() as $error) 
        { 
            error_log("$error", 0);
            echo $error,'<br>';
        }
}
?>
<html>
<head>
    <title>login page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/login.css">
</head>
<body>
    <div class="container-fluid">
        <div class="top">
            <div class="pic">
            <img src="../assets/pics/logo1.png" alt="picture not loaded" class="img-rounded" float="left"></div>
            <h2><i>MINDFIRE</i></h2>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <img src="../assets/pics/logo.png" alt="not loaded" class="img-thumbinal" height="423px" width="400px">
            </div>
            <div class="col-sm-6">
                <div class="log">
                    <div class="img-con">
                        <img src="../assets/pics/u1.png" alt="img not loaded" class="img-rounded">
                    </div>
                    <form action="" method="POST">
                        <strong>Email</strong>
                        <input type="email" name="email" placeholder="enter your username"></br>
                        <strong>Password:</strong>
                        <input type="password" name="password" placeholder="enter your password"></br>
                        <input type="submit" name="login-user">
                        <a href="Forget.php">forget password</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
