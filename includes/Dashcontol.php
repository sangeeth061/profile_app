<?php
$email = $_SESSION['email'];
$firstn = "";
$lastn = "";
$username = "";
$gender = "";
$pass1 = "";
$address = "";
$phnno = "";
$city = "";
$errors = array();

if ($email!="")
{
    $check = Database::getinstance();
    $user =$check->get('user',array('email','=',$email));
    
    if ($user->count())
    {
        foreach ($user->results()as$user) 
        {
            $firstn = escape($user->first_name);
            $lastn = escape($user->last_name);
            $username = escape($user->username);
            $gender = escape($user->gender);
            $phnno = escape($user->mobile_no);
        }   
    }
    $user =$check->get('address',array('email','=',$email));
    
    if ($user->count())
    {
        foreach ($user->results()as$user) 
        {
            $address = escape($user->location);
            $city = escape($user->city);
        }   
    }

}

if (isset($_POST['dash']))
{
    if (Token::check(Input::get('token')))
    {
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
                                                'firstname' => array(
                                                                'valid-name' => 'firstname'
                                                            ),
                                                'confirmpass' => array(
                                                                    'matches' => 'password'
                                                                ),
                                                'lastname' => array(
                                                                'valid-name' => 'lastname'
                                                    )
                                                ) 
                                    );
        if ($validation->passed())
        {
            $check = Database::getinstance();
            $username = escape(Input::get('username'));
            $firstn = escape(Input::get('firstname'));
            $lastn = escape(Input::get('lastname'));
            $gender = escape(Input::get('gender'));
            $address = escape(Input::get('address'));
            $phnno = escape(Input::get('phone'));
            $city = escape(Input::get('city'));
            $pass1 = escape(Hash::make(input::get('password')));
            $pass2 = escape(Hash::make(input::get('confirmpass')));

            $user =$check->update('user',"$email",array(
                                                        "first_name" => "$firstn",
                                                        "last_name" => "$lastn",
                                                        "username" => "$username",
                                                        "gender" => "$gender",
                                                        "password" => "$pass1",
                                                        "mobile_no" => "$phnno"));

            $user =$check->update('address',"$email",array(
                                                        "location" => "$address",
                                                        "city" => "$city"));

            if ($check->count())
            {
                Redirect::to('Dashveiw.php');
            }
            else
            {
                echo 'data is not updated';
            }
        }
        else
        {
            foreach ($validation->errors() as $error) 
            {
                echo $error,'<br>';
                error_log("$error", 0);
            }
            echo 'data1 is not updated';
        }
    }
    else
    {
        echo"intruder";
    }
}   
?>