<?php
    require_once '../Init.php';
    include('Dashcontol.php');
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../assets/css/dashboard.css">
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/additional-methods.min.js"></script>
        <script src="../assets/js/dashboard.js"></script>
    </head>
    <body>
    <header>
        <h2>MINDFIRE</h2>
        <h4>Burn ignorance</h4>
    </header>
        <div class="container-fluid">                
            <h3 align="center">UPDATE THE PERSONAL INFO </h3>
            <form class="form-horizontal" action="Dashveiw.php" method="POST" onsubmit="return validate()">
                <div class="form-group">
                    <label for="firsname" class="col-sm-2 control-label">firstname</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="firstname" placeholder="firstname" name="firstname" value="<?php  if (isset($firstn))
                                            echo"$firstn";
                                            else 
                                            echo " ";?>" required>
                        <span id ="fe" class="highlight"> </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastname" class="col-sm-2 control-label">lastname</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="lastname" placeholder="lastname" name="lastname"
                        value="<?php  if (isset($lastn))
                                                echo"$lastn";
                                                else 
                                                echo " ";?>">
                        <span id ="le" class="highlight"> </span>
                    </div>
                </div>            
                <div class="form-group">
                    <label for="username" class="col-sm-2 control-label">username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="username" placeholder="username" name="username" 
                        value="<?php  if (isset($username))
                                                echo"$username";
                                                else 
                                                echo " ";?>" >
                        <span id ="ue" class="highlight"> </span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 control-label">gender</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="gender" id='gender'>
                            <option name="gender" value="male" >male</option>
                            <option name="gender" value="female">female</option>
                            <option name="gender" value="other">other</option>
                        </select>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" placeholder="password" name="password" 
                        value="">
                        <span id ="pe" class="highlight"> </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="confirmpass" class="col-sm-2 control-label">confirmpass</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="confirmpass" placeholder="confirmpass" name="confirmpass" 
                        value="">
                        <span id ="cpe" class="highlight"> </span>
                    </div>
                </div>              
                <div class="form-group row">
                    <label class="col-sm-2 control-label" for="address">address</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows=3 placeholder="enter address" id="address" name="address" ><?php
                            if (isset($address))
                                echo $address;
                            else 
                                echo ' ';
                            ?></textarea>
                        <span id ="ade" class="highlight"> </span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 control-label" for="phnno">phone&nbsp;<span class="glyphicon glyphicon-phone"></span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="phnno" name="phone" id="phnno"   value=" <?php 
                        if (isset($phnno))
                            echo "$phnno";
                        else
                            echo "";?>">
                        <span id ="phe" class="highlight"> </span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 control-label" for="city">city</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="city" placeholder="city" name="city" value=" <?php 
                        if (isset($city))
                            echo "$city";
                        else
                            echo "";?>">
                    </div>
                </div>
                <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary" name="dash">update</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>