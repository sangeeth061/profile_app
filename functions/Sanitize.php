<?php
/*
Function to remove html entities
params: Strings from input fields;
*/
function escape($string)
{
    return htmlentities($string,ENT_QUOTES,'UTF-8');
}
?>