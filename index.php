<?php 
require_once 'Init.php';
include('Mail.php');

if (Input::exists())
{
    if (Token::check(Input::get('token')))
    {   
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
                                                'username' => array(
                                                                'required1' => true,
                                                                'unique'   => 'user'  
                                                            ),
                                                'email'  => array(
                                                                'required1' => true,
                                                                'unique'   => 'user',
                                                                'valid-email' => 'email'
                                                            ),
                                                'firstname' => array(
                                                                'required1' => true,
                                                                'valid-name' => 'firstname'
                                                            ),
                                                'gender' => array(
                                                                'required1' => true,
                                                            ),
                                                'password' => array(
                                                                'required1' => true,
                                                                'min' => 8
                                                            ),
                                                'confirmpass' => array(
                                                                    'required1' => true,
                                                                    'matches' => 'password'
                                                                )
                                                ) 
                                    );
        if ($validation->passed())
        {
            $mail = new Mail();
            $user = new User();
            try
            {
                $user->create('user',array(
                                'username' => escape(Input::get('username')),
                                'first_name' => escape(Input::get('firstname')),
                                'last_name' => escape(Input::get('lastname')),
                                'gender' => escape(Input::get('gender')),
                                'email' => escape(Input::get('email')),
                                'password' => escape(Hash::make(Input::get('password'))),
                                'mobile_no' => escape(Input::get('phone')),
                                ));
                $user->create('address',array(
                    'email' => escape(Input::get('email')),
                    'city' => escape(Input::get('city')),
                    'location' => escape(Input::get('address'))
                    ));

            }
            catch(Exception $e)
            {
                die($e->getMessage());
            }
                
                $str = "123kkdjkfkjewkjr48u94ifi4309f0ioipc2kf20900420t94igokv";
                $str = str_shuffle($str);
                $str = substr($str,0,10);
                $email = Input::get('email');
                $url = "http://localhost/profileapp_oops/includes/Verification.php?token=$str&email=$email";    
                $mail->sendmail($email,'get started',"to acess the page plzzz click the link:<a href='$url'>'$url</a>" );
                Redirect::to('includes/Alert.php');
                Database::getinstance()->update('user',$email,array('reg_token' => '$str'));
        }
        else
        {
            foreach ($validation->errors() as $error) 
            {
                echo $error,'<br>';
            }
        }
    }   
}
?>
<html>
    <head>
        <title>registrartion page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/register.css">
        
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/additional-methods.min.js"></script>
        <script src="assets/js/register2.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3 align="center">Registration page </h3>
                    <form class="form-horizontal container registartion-form" action="" method="POST" style="padding-top: 10px;" name="test" id="test" onsubmit="return validate()">
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="firstname">firstname <span id="star">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="firstname" id="firstname" name="firstname" value="<?php echo Input::get('firstname');?>">
                                <span id ="fe" class="highlight"> </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="lastname">lastname</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="lastname" id="lastname" name="lastname" value="<?php echo Input::get('lastname'); ?>" >
                                <span id = "le" class="highlight"> </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="username">username<span id="star">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="username" id ="username" name="username" required1 value="<?php echo Input::get('username'); ?>"> 
                                <span id = "ue" class="highlight"> </span>      
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 control-label">gender</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="gender" id='gender'>
                                    <option name="gender" value="male" >male</option>
                                    <option name="gender" value="female">female</option>
                                    <option name="gender" value="other">other</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="email">email&nbsp;<span class="glyphicon glyphicon-envelope"></span><span id="star">*</span></label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" placeholder="email" name="email" id="email" required1 value="<?php echo Input::get('email'); ?>" autocomplete="off">
                                <span id ="ee" class="highlight"> </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="password">password<span id="star">*</span></label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" placeholder="password" name="password" id="password" required1 value="<?php echo Input::get('password'); ?>">
                                <span id="pe" class="highlight"> </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="confirm">confirm password<span id="star">*</span></label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" placeholder="type your password" id="confirm" 
                                name="confirmpass" required1 value="<?php echo Input::get('confirmpass'); ?>">
                                <span id='cpe' class="highlight"> </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="address">address<span id="star">*</span></label>
                            <div class="col-sm-8">
                                <textarea class="form-control" rows=3 placeholder="enter address" id="address" name="address" required1><?php echo Input::get('address'); ?></textarea>
                                <span id="ade" class="highlight"> </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="phnno">phone&nbsp;<span class="glyphicon glyphicon-phone"></span><span id="star">*</span></label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" placeholder="phnno" name="phone" id="phnno" required1 value="<?php echo Input::get('phone'); ?>">
                                <span id ="phe" class="highlight"> </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="city">city</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="city" placeholder="city" name="city">
                            </div>
                        </div>
                        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="submit" class="btn btn-primary" placeholder="REGISTER" name="regis" value="Register">
                                <p style="float :left;  ">Already user ?<a href="includes/Login.php"> click here</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>     