$(document).ready(function(){
  $.validator.setDefaults({
    errorClass:"help-block",
    highlight:function(element){
      $(element).closest(".form-group").addClass("has-error");
    },
    unhighlight:function(element){
      $(element).closest(".form-group").removeClass("has-error");
    }
  })
  $("#test").validate({
    rules:{
     
      firstname:
      {
        required:true,
      },
      username:
      {
      	required:true,
      },
      password:
      {
      	required:true,
      	minlength:8,
      },
      email:
      {
      	required:true,
      	email:true,
      },
     confirmpass:
      {
      	required:true,
      	equalTo:"#password",
      },
      phone:
      {
      	required:true,
      },

    },
    messages:{
      username:
      {
      	required:"please fill the field",
      },
      password:
      {
      	required:"please fill the field",
      },
      confirmpass:
      {
      	required:"please fill the field",
      	equalTo:"incorrect password"
      },
      email:
      {
      	required:"please fill the feild",

      },
      firstname:
      {
        required:"Please fill this field",
      },
    }
  });


$("input[name='test']").click(function(event){
  return $("#test").valid();
})

})

function checkmail()
{
  var email1=document.getElementById("email").value;
  if(email1)
  {
    $.ajax({
      type:'post',
      url:'../../classes/Checkmail.php',
      data:{
        email:email1,
      },
      success: function (response){
        $('#ee').html(response);
        if(response=="ok")
        {
          return true;
        }
        else
        {
        return false;
      }
      }
    })
  }
  else
  {
    $('#ee').html("hi");
    return false;
  }
}





function validate() 
{
	// echo"hi"x;
var username = document.getElementById('username').value;
var firstname = document.getElementById('firstname').value;
var lastname = document.getElementById('lastname').value;;
var email = document.getElementById('email').value;;
var gender = document.getElementById('gender').value;
// console.log("gender");
var password1 = document.getElementById('password').value;
var password2 = document.getElementById('confirm').value;
var address = document.getElementById('address').value;
var phoneno = document.getElementById('phnno').value;
var city = document.getElementById('city').value;

var error = 0;

if(username == "")
{
	alert("username required");
	document.getElementById('ue').innerHTML = "username is required";
	error = 1;
}

if(firstname == "")
{
	document.getElementById('fe').innerHTML="firstname is required";
	error = 1;
}

if((firstname.search(/^[a-zA-Z]+$/))<0)
{
	document.getElementById('fe').innerHTML="only alphabets";
	error = 1;
}

if(email == "")
{
	document.getElementById('ee').innerHTML="email is required";
	error = 1;
}

if((email.search(/^[a-zA-Z][a-z A-Z 0-9]*@[a-zA-z]+\.[a-zA-z]+$/))<0)
{
	error = 1;
	document.getElementById('ee').innerHTML="enter in the emailformat";
}

if(password1 == "")
{
	error = 1;
	document.getElementById('pe').innerHTML="password is required";
}

if(password2 == "")
{
	error = 1;
	document.getElementById('cpe').innerHTML="confirm password is required";
}

if(strlen(password2)<8)
{
	error = 1;
	document.getElementById('cpe').innerHTML="confirm_password should contain atleast 8 characters";
}

if(password1 != password2)
{
	error = 1;
	document.getElementById('cpe').innerHTML="please check your input";
}

if(address == "")
{
	error = 1;
	document.getElementById('ae').innerHTML="address is required";
}

if(phoneno == "")
{
	error = 1;
	document.getElementById('pe').innerHtml="phone no is required";
}
if(!phoneno == 10)
{
	error = 1;
	document.getElementById('pe').innerHTML="enter valid no";
}
console.log(error);
if(error == 1)
{
	return false;
}
else
{
	return true;
}
}
