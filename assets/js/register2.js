var ALPHABETS = new RegExp(/^[a-zA-Z ]+$/);
var NUMBERS = new RegExp(/^[0-9]+$/);
var EMAIL = new RegExp(/^[a-zA-Z][a-z A-Z 0-9]*@[a-zA-z]+\.[a-zA-z]+$/);
$(document).ready(function(){

	$("[name = 'firstname']").blur(function()
	{
		var firstname = $(this).val();

		if (firstname === "") 
		{
			$("#fe").html("First name is required.");
		} 
		else if (!(ALPHABETS.test(firstname))) 
		{
			$("#fe").html("Alphabets only.");
			$("[name = 'firstname']").focus();
		}
		else
		{
			$("#fe").html("");
		}
	});

	$("[name = 'email']").blur(function()
	{
		var email1 = $(this).val();

		if (email1 === "")
		{
			$("#ee").html("Email is required.");
			$("[name = 'email']").focus();
		} 
		else if (!(EMAIL.test(email1))) 
		{
			$("#ee").html("Invalid email.");
			$("[name = 'email']").focus();
		}
		else
		{
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() 
			{
				if (this.readyState == 4 && this.status == 200) 
				{
					document.getElementById('ee').innerHTML = this.responseText;
				}
			};
			xhttp.open("POST", "../includes/Checkmail.php", true);
			xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhttp.send("email="+email1);
		}
	});

	$("[name = 'password']").keyup(function()
	{
		var pwd = $(this).val();

		if (pwd === "") 
		{
			$("#pe").html("Password is required.");
			$("[name = 'password']").focus();
		} 
		else if (pwd.length < 8) 
		{
			$("#pe").html("Min 8 characters.");
			$("[name = 'password']").focus();
		}
		else
		{
			$("#pe").html("");
		}
	});

	$("[name = 'confirmpass']").keyup(function()
	{
		var pwd_confirm = $(this).val();

		if (pwd_confirm === "") 
		{
			$("#cpe").html("Confirm the password.");
			$("[name = 'confirmpass']").focus();
		} 
		else if (pwd_confirm !== $("[name = 'password']").val()) 
		{
			$("#cpe").html("Passwords did NOT match");
			$("[name = 'confirmpass']").focus();
		}
		else
		{
			$("#cpe").html("");
		}
	});

	$("[name = 'address']").blur(function()
	{
		var address = $(this).val();

		if (address === "") 
		{
			$("#ade").html("adress is required");
			$("[name = 'address']").focus();
		} 
		else
		{
			$("#ade").html("");
		}
	});

	$("[name = 'phone']").blur(function()
	{
		var phone = $(this).val();

		if (phone === "") 
		{
			$("#phe").html("phone is required.");
			$("[name = 'phone']").focus();
		} 
		else if (phone.length != 10) 
		{
			$("#phe").html("invalid no");
			$("[name = 'phone']").focus();
		}
		else if(!(NUMBERS.test(phone)))
		{
			$("#phe").html("invalid no");
			$("[name = 'phone']").focus();
		}
		else
		{
			$("#phe").html("");
		}
	});
});

function validate() 
{
var username = document.getElementById('username').value;
var firstname = document.getElementById('firstname').value;
var lastname = document.getElementById('lastname').value;
var email = document.getElementById('email').value;
var gender = document.getElementById('gender').value;
var password1 = document.getElementById('password').value;
var password2 = document.getElementById('confirm').value;
var address = document.getElementById('address').value;
var phoneno = document.getElementById('phnno').value;
var city = document.getElementById('city').value;

var error = 0;

if(username == "")
{
	document.getElementById('ue').innerHTML = "usernames is required";
	error = 1;
}

if(firstname == "")
{
	document.getElementById('fe').innerHTML="firstname is required";
	error = 1;
}

if((firstname.search(/^[a-zA-Z]+$/))<0)
{
	document.getElementById('fe').innerHTML="only alphabets";
	error = 1;
}

if(email == "")
{
	document.getElementById('ee').innerHTML="email is required";
	error = 1;
}

if((email.search(/^[a-zA-Z][a-z A-Z 0-9]*@[a-zA-z]+\.[a-zA-z]+$/))<0)
{
	error = 1;
	document.getElementById('ee').innerHTML="enter in the emailformat";
}

if(password1 == "")
{
	error = 1;
	document.getElementById('pe').innerHTML="password is required";
}

if(password2 == "")
{
	error = 1;
	document.getElementById('cpe').innerHTML="confirm password is required";
}

if(password1 != password2)
{
	error = 1;
	document.getElementById('cpe').innerHTML="please check your input";
}

if(address == "")
{
	error = 1;
	document.getElementById('ade').innerHTML="address is required";
}

if(phoneno == "")
{
	error = 1;
	document.getElementById('phe').innerHtml="phone no is required";
}
if(!phoneno == 10)
{
	error = 1;
}
if(error == 1)
{
	return false;
}
else
{
	return true;
}
}
