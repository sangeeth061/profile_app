var ALPHABETS = new RegExp(/^[a-zA-Z ]+$/);
var NUMBERS = new RegExp(/^[0-9]+$/);
var error =0;
$(document).ready(function(){

	$("[name = 'firstname']").blur(function()
	{
		var firstname = $(this).val();

		if (!(ALPHABETS.test(firstname))) 
		{
			$("#fe").html("Alphabets only.");
			$("[name = 'firstname']").focus();
		}
		else
		{
			$("#fe").html("");
		}
	});

	$("[name = 'password']").keyup(function()
	{
		var pwd = $(this).val();

		if (pwd.length < 8) 
		{
			$("#pe").html("Min 8 characters.");
			$("[name = 'password']").focus();
		}
		else
		{
			$("#pe").html("");
		}
	});

	$("[name = 'confirmpass']").keyup(function()
	{
		var pwd_confirm = $(this).val();

		if (pwd_confirm === "") 
		{
			$("#cpe").html("Confirm the password.");
			$("[name = 'confirmpass']").focus();
		} 
		else if (pwd_confirm !== $("[name = 'password']").val()) 
		{
			$("#cpe").html("Passwords did NOT match");
			$("[name = 'confirmpass']").focus();
		}
		else
		{
			$("#cpe").html("");
		}
	});

	$("[name = 'phone']").blur(function()
	{
		var phone = $(this).val();

		if (phone.length != 10) 
		{
			$("#phe").html("invalid no");
			$("[name = 'phone']").focus();
		}
		else if(!(NUMBERS.test(phone)))
		{
			$("#phe").html("invalid no");
			$("[name = 'phone']").focus();
		}
		else
		{
			$("#phe").html("");
		}
	});
});

function validate() 
{
var username = document.getElementById('username').value;
var firstname = document.getElementById('firstname').value;
var password1 = document.getElementById('password').value;
var password2 = document.getElementById('confirmpass').value;
var phoneno = document.getElementById('phnno').value;
console.log("hi");
var error=0;

if(username == "")
{
	document.getElementById('ue').innerHTML = "usernames is required";
	error = 1;
}

if((firstname.search(/^[a-zA-Z]+$/))<0)
{
	document.getElementById('fe').innerHTML="only alphabets";
	error = 1;
}

if(password1 != password2)
{
	error = 1;
	document.getElementById('cpe').innerHTML="please check your input";
}

if((phoneno.search(/^[0-9]+$/))<0)

if(!phoneno == 10)
{
	error = 1;
}

if(error == 1)
{
	return false;
}
else
{
	return true;
}
}
