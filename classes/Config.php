<?php

//It gets the senstive or common data
class Config
{
    /**
     * To get senstive data
     *
     * @access public
     * @param  string $path
     * @return boolean
     */
    
    public static function get($path = null)
    {
        if ($path)
        {
            $config = $GLOBALS['config'];
            $path = explode('/' , $path);
            foreach ($path as $bit)     
            {
                if (isset($config[$bit]))
                {
                    $config = $config[$bit];
                }
            }

                return $config;
        }

        return false;
    }
}