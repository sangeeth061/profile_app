<?php
//Registring user and checking credintals
class User
{
    private $_db;
    private $_data;
    private $_sessionname;

    public function __construct($user = null)
    {
        $this->_db = Database::getinstance();
        $this->_sessionname = Config::get('session/session_name');
    }

    /**
     * To register user
     *
     * @access public
     * @param  array $fields
     * @return boolean
     */
    public function create($table,$fields = array())
    {
        if (!$this->_db->insert($table,$fields))
        {
            error_log('Data not entered', 0);
            throw new Exception("data is not entered");
        }
    }

    /**
     * To get user details
     *
     * @access public
     * @param  string $user
     * @return boolean
     */
    public function find($user = null)
    {
        if ($user)
        { 
            $field = 'email';
            $data = $this->_db->get('user', array($field , '=', $user));

            if ($data->count())
            {
                $this->_data = $data->first();
                return true;
            }
        }

        return false;
    }

    /**
     * To login user
     *
     * @access public
     * @param  string $email
     * @param  string $password
     * @return boolean
     */
    public function login($email = null, $password = null)
    {
        $user = $this->find($email);
        
        if ($user)
        {
            if ($this->data()->password === Hash::make($password))
            {
                if ($this->data()->email_verified == '1')
                {
                    Session::put($this->_sessionname, $this->data()->email);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * to access result set
     *
     * @access public
     * @return object
     */
    private function data()
    {
        return $this->_data;
    }
}