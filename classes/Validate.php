<?php
//validating the values
class Validate
{
    private $_passed = false ;
    private $_errors =array();
    private $_db = null;
    
    public function __construct()
    {
        $this->_db = Database::getinstance(); 
    } 
    
    /**
     * To verify user
     *
     * @access public
     * @param  string $source
     * @param  array $items
     */
   
    public function check($source, $items = array())
    {
        foreach ($items as $item => $rules) 
        {
            $x = 1;//used for looping

            foreach ($rules as $rule => $rule_value)
            {
                $value = $source[$item];

                switch($rule)
                {
                    case 'required1': if (empty($value))
                                     {
                                        $this->adderror("{$item} is required");
                                     }
                    break;
                    case 'min': if (strlen($value)<$rule_value)
                                {
                                    $this -> adderror("{$item} must be minimum of {$rule_value}");
                                }
                    break;
                    case 'matches': if ($value != $source[$rule_value])
                                    {
                                        $this -> adderror("{$item} dosent match");
                                    }
                    break;
                    case 'unique': $check1 = $this->_db->get($rule_value,array($item,'=',$value));
                                    if ($check1->count())
                                    {
                                        $this -> adderror("{$item} all ready exist");
                                    }
                    break;
                    case 'valid-email': if (!preg_match("/^[a-zA-Z]+[a-zA-Z0-9]*@[a-zA-Z]+\.[a-zA-Z]+$/",$source[$rule_value]))
                                        {
                                            $this -> adderror("{$item} dosent match");
                                        }
                    break;
                    case 'valid-name': if (!preg_match("/^[a-zA-Z]+$/",$source[$rule_value]))
                                        {
                                            $this -> adderror("{$item} dosent match");
                                        } 
                    break;
                }
            }
        }

        if (empty($this->_errors))
        {
            $this->_passed = true;
        }

        return $this;
    }
    
    /**
     * To indicate error
     *
     * @access private
     * @param  string $error
     * @return boolean
     */
    
    private function adderror($error)
    {
        $this->_errors[] = $error;
    }

    public function errors()
    {
        return $this->_errors;
    }
   
    /**
     * To verify the result set
     *
     * @access public
     * @return boolean
     */
   
    public function passed()
    {
        return $this->_passed;
    }
}