<?php
//sends the data 
class Input
{
    /**
     * To check whether request is made or not
     *
     * @access public
     * @param  string $type
     * @return boolean
     */ 
    
    public static function exists($type = 'POST')
    {
        switch ($type)
        {
            case 'POST':
                return (!empty($_POST)) ? true : false ;
                break;

            case 'GET':
                return (!empty($_GET)) ? true :false ;
                break;

            default :
                return false ;
                break ;
        }
    }
    
    /**
     * To Get values
     *
     * @access public
     * @param  string $item
     * @return string
     */
    
    public static function get($item)
    {
        if (isset($_POST[$item]))
        {
            return $_POST[$item];
        }
        elseif (isset($_GET[$item])) 
        {
            return $_GET[$item];
        }
    }
}