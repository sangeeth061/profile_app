<?php
class Redirect
{
    /**
     * To Redirect to specific location
     *
     * @access public
     * @param  string $location
     */
   
    public static function to($location = null)
    {
        if ($location)
        {
            header('location:' . $location);
            exit();
        }
    }
}
