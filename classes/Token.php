<?php
//to check for csrf
class Token
{
    /**
     * To generate token
     *
     * @access public
     * @return string
     */
    
    public static function generate()
    {
        return Session::put(Config::get('session/token_name'),md5(uniqid()));
    }
    
    /**
     * To verify token
     *
     * @access public
     * @param  string $token
     * @return boolean
    
     */
    
    public static function check($token)
    {
        $tokenname = Config::get('session/token_name');

        if (Session::exists($tokenname) && $token === Session::get($tokenname))
        {
            Session::delete($tokenname);
            
            return true;
        }

        return false;
    }
}