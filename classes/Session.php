<?php
//Control the session
class Session
{
    /**
     * To assign Session value
     *
     * @access public
     * @param  string $name
     * @param  string $value
     * @return string
     */
    
    public static function put($name,$value)
    {
        return $_SESSION[$name] = $value;
    }
    
    /**
     * To check session exist or not
     *
     * @access public
     * @param  string $name
     * @return boolean
     */
    
    public static function exists($name)
    {
        return (isset($_SESSION[$name])) ? true : false;
    }
    
    /**
     * To delete session
     *
     * @access public
     * @param  string $name
     */
    
    public static function delete($name)
    {
        if (self::exists($name))
        {
            unset($_SESSION[$name]);
        }
    }
    
    /**
     * To get value
     *
     * @access public
     * @param  string $name
     * @return string
     */
    
    public static function get($name)
    {
        return $_SESSION[$name];
    } 
}