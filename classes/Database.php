<?php
//Performs database operations
class Database
{
    private static $_instance = null;
    private $_pdo, $_query, $_results, $_error = false, $_count = 0;

    /*
    Creates database object 
    access modifier : private
    */
    private function __construct()
    {
        try 
        {
            $this->_pdo = new PDO('mysql:host=' . Config::get('mysql/host') .';dbname='.Config::get('mysql/Database') ,Config::get('mysql/username'),Config::get('mysql/password'));
        }
        catch(PDOException $e)
        {
            error_log('database not available!', 0);
            die($e->getMessage());
        }
    }

    /**
     * To check whether objectis created or not
     *
     * @access public
     * @return object
     */
    public static function getinstance()
    {
        if (!isset(self::$_instance))
        {
            self::$_instance = new Database();
        }

        return self::$_instance;
    }

    /**
     * To execute query
     *
     * @access public
     * @param  string $sql
     * @param  array $params
     * @return object
     */
    public function query($sql, $params = array() )
    {
        $this->_error = false;
        
        if ($this->_query = $this->_pdo->prepare($sql))
        {
            $x = 1;
           
            if (count($params))
            {
                foreach($params as $param)
                {
                    $this->_query->bindvalue($x,$param);
                    $x++;
                }
            }
           
            if ($this->_query->execute())
            {
                $this->_results = $this->_query->fetchALL(PDO::FETCH_OBJ);
                $this->_count = $this->_query->rowCount();
            }
           
            else
            {   
                print_r($this->_query->errorInfo());
                $this->_error = true;
            }
        }
        return $this;
    }

    /**
     * To prepare query
     *
     * @access public
     * @param  string $action
     * @param  string $table
     * @param  array  $where
     * @return object
     */
    public function action($action, $table, $where = array())
    {
        if (count($where) === 3)
        {
            $operators = array('=', '>', '<', '<=', '!=', '>=');
            $field = $where[0];
            $operator = $where[1];
            $value = $where[2];
            
            if (in_array($operator, $operators))
            {
                $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";
                
                if (!$this->query($sql, array($value))->error())
                {
                    return $this;
                }
            }
        }

        return false;
    }

    /**
     * To prepare sql statement
     *
     * @access public
     * @param  string $table
     * @param  array $where
     * @return object
     */
    public function get($table,$where)
    {
        return $this->action('select*', $table, $where);
    }

    /**
     * To indicate error
     *
     * @access public
     * @return object
     */
    public function error()
    {
        return $this->_error;
    }

    /**
     * To indicate no of rows
     *
     * @access public
     * @return object
     */
    public function count()
    {
        return $this->_count;
    }

    /**
     * To store result
     *
     * @access public
     * @return object
     */
    public function results()
    {
        return $this->_results;
    }

    /**
     * To get first result
     *
     * @access public
     * @return object
     */
    public function first()
    {
        return $this->_results[0];
    }

    /**
     * To prepare update sql statement
     *
     * @access public
     * @param  string $table
     * @param  array $fields
     * @param  string $email
     * @return boolean
     */
    public function update($table,$email,$fields)
    {
        $set = '';
        $x = 1 ;

        foreach ($fields as $name => $value) 
        {
            $set .= "{$name} = ?";

            if ($x < count($fields))
            {
                $set .= ',';
            }

            $x++;   
        }

        $sql = "update {$table} set {$set} where email='$email'";

        if (!$this->query($sql,$fields)->error())
        {
            return true;
        }

        return false;
    }

    /**
     * To prepare update insert statement
     *
     * @access public
     * @param  string $table
     * @param  array $fields
     * @return boolean
     */
    public function insert ($table,$fields=array())
    {   
        if (count($fields))
        {
            $key = array_keys($fields);
            $values = '';
            $x = 1;

            foreach($fields as $field) 
            {
                $values .="?";

                if ($x < count($fields))
                {
                    $values.=",";
                }

                $x++;
            }

            $sql = "insert into {$table} (`".implode('`,`', $key)."`) VALUES ({$values})";
            
            if (!$this->query($sql,$fields)->error())
            {   
                return true;
            }
            return false;
        }
    }
}
